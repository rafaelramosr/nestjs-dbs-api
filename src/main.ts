import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LowDbService } from './services';

async function bootstrap() {
  const connection = new LowDbService(new ConfigService());
  await connection.createDatabase();
  const app = await NestFactory.create(AppModule, { cors: true });
  await app.listen(3000);
}
bootstrap();
