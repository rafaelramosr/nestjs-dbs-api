import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';

import { AppModule } from './app.module';
import { LowDbService } from './services';

const createNestServer = async (expressInstance: any) => {
  const connection = new LowDbService(new ConfigService());
  await connection.createDatabase();
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(expressInstance),
  );
  app.enableCors();
  return app.init();
};

export default createNestServer;
