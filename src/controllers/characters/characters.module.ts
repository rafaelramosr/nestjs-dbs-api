import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { LowDbService } from '../../services';
import { CharactersService } from './characters.service';
import { CharactersController } from './characters.controller';

@Module({
  controllers: [CharactersController],
  providers: [CharactersService, ConfigService, LowDbService],
})
export class CharactersModule {}
