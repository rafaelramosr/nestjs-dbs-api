import { Injectable } from '@nestjs/common';
import { LowDbService } from 'src/services';
import { ModelName } from 'src/services/interfaces';

@Injectable()
export class CharactersService {
  private modelName: ModelName = 'characters';

  constructor(private readonly lowDbService: LowDbService) {}

  findAll() {
    return this.lowDbService.getAll(this.modelName);
  }

  findOne(id: string) {
    return this.lowDbService.getOneById(this.modelName, id);
  }
}
