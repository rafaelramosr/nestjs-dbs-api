import * as lowdb from 'lowdb';

export type ModelName = 'characters' | 'locations';
export type LowDB = lowdb.LowdbSync<DatabaseSchema>;

export interface CharacterModel {
  id: string;
  name: string;
  race: string;
  bio: string;
  img?: string;
  images: { sm: string; md: string; lg: string };
}

export interface DatabaseSchema {
  characters: CharacterModel[];
  locations: { id: string; name: string }[];
}
