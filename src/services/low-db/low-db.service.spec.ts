import { Test, TestingModule } from '@nestjs/testing';
import { LowDbService } from './low-db.service';

describe('LowDbService', () => {
  let service: LowDbService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LowDbService],
    }).compile();

    service = module.get<LowDbService>(LowDbService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
