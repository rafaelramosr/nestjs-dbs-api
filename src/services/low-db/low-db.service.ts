import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as uuid from 'uuid';
import fetch from 'node-fetch';
import * as lowdb from 'lowdb';
import * as FileSync from 'lowdb/adapters/FileSync';

import {
  CharacterModel,
  DatabaseSchema,
  LowDB,
  ModelName,
} from '../interfaces';

@Injectable()
export class LowDbService {
  private db: LowDB;
  private imagePath: string = this.configService.get<string>('IMAGE_PATH');

  constructor(private configService: ConfigService) {}

  public async createDatabase(): Promise<void> {
    const db = this.getConnection();
    if (Object.keys(db.getState()).length) return;

    fetch(this.configService.get<string>('ORIGINAL_JSON_CHARACTERS'))
      .then((res: any) => res.json())
      .then((characters: CharacterModel[]) => {
        db.defaults({
          characters: characters.map((character) => ({
            id: uuid.v4(),
            name: character.name,
            race: character.race,
            bio: character.bio,
            images: {
              sm: `${this.imagePath}sm/${character.img.split('/')[1]}`,
              md: `${this.imagePath}md/${character.img.split('/')[1]}`,
              lg: `${this.imagePath}lg/${character.img.split('/')[1]}`,
            },
          })),
        }).write();
      });
  }

  private getConnection(): LowDB {
    const adapter = new FileSync<DatabaseSchema>('db.json');
    this.db = lowdb(adapter);
    return this.db;
  }

  public getAll(modelName: ModelName): DatabaseSchema[ModelName] {
    return this.getConnection().get(modelName).value();
  }

  public getOneById(
    modelName: ModelName,
    id: string,
  ): DatabaseSchema[ModelName][number] | undefined {
    return this.getConnection().get(modelName).find({ id }).value();
  }
}
