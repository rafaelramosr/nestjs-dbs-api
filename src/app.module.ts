import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

// Controllers
import { AppController } from './app.controller';
import { CharactersModule } from './controllers';

// Services
import { AppService } from './app.service';
import { LowDbService } from './services';

@Module({
  imports: [
    CharactersModule,
    ConfigModule.forRoot({
      envFilePath: ['.env.development.local', '.env.development'],
    }),
  ],
  controllers: [AppController],
  providers: [AppService, LowDbService],
})
export class AppModule {}
